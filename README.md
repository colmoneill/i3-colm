# i3 config colm

current setup manjaro 17 gnome dl

general config `/home/colm/.config/i3/config`

According man 1 i3 i3 looks in the following places - and in that order - for a configuration file:

    ~/.config/i3/config (or $XDG_CONFIG_HOME/i3/config if set)
    /etc/xdg/i3/config (or $XDG_CONFIG_DIRS/i3/config if set)
    ~/.i3/config
    /etc/i3/config

i3status config `/etc/i3status.conf`

mostly used https://github.com/jakehappersett/X220-i3-config as 
ref 
